<?php
/**
* Bigawuc Wordpress function and definitions
*
* @package bigawuc
*/

if ( ! function_exists( 'bigawuc_setup' ) ) :

	function bigawuc_setup() {

		/**
		* Add default posts and comments RSS feed links to head
		*/
		add_theme_support( 'automatic-feed-links' );

		/**
		* Enable support for Post Thumbnails
		*/
		add_theme_support( 'post-thumbnails');
	
	}
endif; // bigawuc_setup
add_action( 'after_setup_theme', 'bigawuc_setup' );

/**
* Enqueue scripts and styles
*/
function bigawuc_scripts_and_styles() {
	wp_enqueue_style( 'style', get_stylesheet_uri() );
}
add_action( 'wp_enqueue_scripts', 'bigawuc_scripts_and_styles' );